# Radicale

Wrapper of [tomsquest/docker-radicale](https://github.com/tomsquest/docker-radicale) with [matrix auth](https://gitlab.com/etke.cc/radicale-auth-matrix) module included
